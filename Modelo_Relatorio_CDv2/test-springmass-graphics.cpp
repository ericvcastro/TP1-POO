/** file: test-springmass-graphics.cpp
 ** brief: Tests the spring mass simulation with graphics
 ** author: Andrea Vedaldi
 **/

#include "graphics.h"
#include "springmass.h"

#include <iostream>
#include <sstream>
#include <iomanip>

/* ---------------------------------------------------------------- */
class SpringMassDrawable : public SpringMass, public Drawable
/* ---------------------------------------------------------------- */
{
/* INCOMPLETE: TYPE YOUR CODE HERE */
 private:
 	Figure figure;


 public:
 	SpringMassDrawable(): figure("Spring and Mass"){
 		figure.addDrawable(this);
 	}
 	
 	void draw(){
 		for(int i=0; i<vector_mass.size();i++){

			cout << vector_mass[i]->getRadius() << "raio" << std::endl;
 			figure.drawCircle(vector_mass[i]->getPosition().x, vector_mass[i]->getPosition().y, vector_mass[i]->getRadius());
 		}
 		
 		for(int i=0; i<vector_spring.size();i++){
 			
 			figure.drawLine(vector_spring[i]->getMass1()->getPosition().x, vector_spring[i]->getMass1()->getPosition().y,
 							vector_spring[i]->getMass2()->getPosition().x, vector_spring[i]->getMass2()->getPosition().y, 
 							0.05);
 		
 		}
 	
 	}
 	


 	void display(){
 		figure.update();
 	}
};

int main(int argc, char** argv)
{
  glutInit(&argc,argv) ;
  const double dt = 1/60.0 ;
  SpringMassDrawable springmass;

  /* INCOMPLETE: TYPE YOUR CODE HERE */

  const double mass = 0.1 ;
  const double radius = 0.05 ;
  const double naturalLength = 0.02 ;
  const double stiffness = 1.0;

  Mass m1(Vector2(+0.9,0.3), Vector2(), mass, radius) ;
  Mass m2(Vector2(-0.9,0.1), Vector2(), mass, radius) ;

  Spring spring(&m1, &m2, naturalLength, stiffness);
  springmass.massADD(&m1);
  springmass.massADD(&m2);
  springmass.springADD(&spring);

  run(&springmass, 1/120.0) ;
  return 0;
}
