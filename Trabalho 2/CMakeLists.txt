cmake_minimum_required(VERSION 2.8)
project(system_entrance)

find_package(OpenCV REQUIRED)
add_executable(${PROJECT_NAME} main.cpp detecter_face.cpp System_Entrace.hpp)
target_link_libraries(${PROJECT_NAME} ${OpenCV_LIBS} )

