#ifndef _SYSTEM_ENTRACE_
#define _SYSTEM_ENTRACE_

 #include <iostream>
 #include <stdio.h>
 #include <stdlib.h>
 #include <string>
 #include <opencv2/opencv.hpp>
 #include <opencv2/highgui.hpp>
 #include "opencv2/objdetect/objdetect.hpp"
 #include "opencv2/highgui/highgui.hpp"
 #include "opencv2/imgproc/imgproc.hpp"
 #include "opencv2/core/core.hpp"
 #include <cv.h>



using namespace std;
using namespace cv;


class System
{
	public:
        const double PERCENT_MATCH = 0.9;
		string face_cascade_name; // String para receber a rede neural do rosto
		string eyes_cascade_name; // String para receber a rede neural dos olhos
		string window_name;
    
    	System() ;
        void samePerson(Mat frame, string person);
        void detectAndDisplay(Mat frame, CascadeClassifier face_cascade, CascadeClassifier eyes_cascade);
        void Display(System *sistema, CascadeClassifier face_cascade, CascadeClassifier eyes_cascade);


	private:

	
	protected:

		

};
#endif /*Definido a _SYSTEM_ENTRANCE_*/
