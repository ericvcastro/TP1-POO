#include "System_Entrace.hpp"

 using namespace std;
 using namespace cv;

System::System(): face_cascade_name("haarcascade_frontalface_alt.xml"), eyes_cascade_name("haarcascade_eye_tree_eyeglasses.xml"),
 window_name("Capture - Face detection")
{}

/*void System::samePerson(Mat frame, string person){

    int noMatchCount = 0;
    for (int x = 0; x < frame; x++)
    {
        for (int y = 0; y < person; y++)
        {
           if ( !pixelsMatch( image.GetPixel(x,y), srClickedArray[x%16, y%16] )
           {
               noMatchCount++;
               if ( noMatchCount > ( 16 * 16 * ( 1.0 - PERCENT_MATCH ))
                  goto matchFailed;
           }
        }
    }
    Console.WriteLine("images are >=90% identical");
    return;
    matchFailed:
    Console.WriteLine("image are <90% identical");
}*/

void System::detectAndDisplay( Mat frame, CascadeClassifier face_cascade, CascadeClassifier eyes_cascade)
{
    string person = "image/eric.jpeg";
    vector<Rect> faces;

    Mat frame_gray;// = cv::Mat::zeros(vsrc.size(), CV_8UC1);
    cvtColor( frame, frame_gray, CV_BGR2GRAY );
    equalizeHist( frame_gray, frame_gray );

    //-- Detect faces
    face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );
    
    for( size_t i = 0; i < faces.size(); i++ )
    {
      Point center( faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5 );
      ellipse( frame, center, Size( faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, Scalar( 255, 0, 255 ), 4, 8, 0 );
      Mat faceROI = frame_gray( faces[i] );
      std::vector<Rect> eyes;

      //-- In each face, detect eyes
      eyes_cascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0 |CV_HAAR_SCALE_IMAGE, Size(30, 30) );

      for( size_t j = 0; j < eyes.size(); j++ )
       {
         Point center( faces[i].x + eyes[j].x + eyes[j].width*0.5, faces[i].y + eyes[j].y + eyes[j].height*0.5 );
         int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
         circle( frame, center, radius, Scalar( 255, 0, 0 ), 4, 8, 0 );

         if(radius > 15){
             vector<int> compression_params;
             compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
             compression_params.push_back(100);
             imwrite(person, frame, compression_params);

         }

       }
    }
    //-- Show what you got
    imshow( window_name, frame );

 }

void System::Display(System *sistema, CascadeClassifier face_cascade, CascadeClassifier eyes_cascade){

    VideoCapture capture(0);
    Mat frame;
       //-- 1. Load the cascades
      if( !face_cascade.load( sistema->face_cascade_name ) )
      {
         printf("--(!)Error loadingFACE\n"); 
         return;
      } 


      
      if( !eyes_cascade.load( sistema->eyes_cascade_name ) )
      {
        printf("--(!)Error loadingEYES\n");
        return; 
      }


       //sistema->face_cascade = &face_cascade;
       //sistema->eyes_cascade = &eyes_cascade;


       //-- 2. Read the video stream



      
       if(capture.isOpened())
       {
         while( true )
         {
          
          
          capture.read(frame);

       //-- 3. Apply the classifier to the frame
           if( !frame.empty() )
           { 
              sistema->detectAndDisplay(frame, face_cascade, eyes_cascade);
           }

           else
             {
              printf(" --(!) No captured frame -- Break!"); 
              break; 
             }

           waitKey(10);
        }
      }
      return;
 }

